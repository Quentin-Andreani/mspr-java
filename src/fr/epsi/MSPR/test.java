package fr.epsi.MSPR;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalTime;
import java.util.ArrayList;

public class test {

    public static void TestIfTxtHasAllData(ArrayList<String> Erreurs, String[] InfoSalarie) throws Exception {
        if(InfoSalarie[0].isEmpty() || InfoSalarie[1].isEmpty()|| InfoSalarie[2].isEmpty() || InfoSalarie[3].isEmpty()){
            throw new Exception("Probleme dans les donnees agent du fichier");
        }
        if(!InfoSalarie[4].equals("")){
            throw new Exception("La ligne 5 n'est pas vide");
        }
    }
    public static void TestIfFileAreImage(ArrayList<String> Erreurs,String[] Image, String AgentPhotoDirectory) {
        for (String image : Image) {
            Path path = new File(AgentPhotoDirectory+image).toPath();
            String mimeType = null;
            try {
                mimeType = Files.probeContentType(path);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (!mimeType.equals("image/jpeg")) {
                Erreurs.add("Le fichier"+image+"n'est pas une image");
            }
        }
    }
}
